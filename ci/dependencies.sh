#!/usr/bin/env sh

# Update apk
apk update

# Add python pip and bash
apk add --no-cache py-pip bash

# Install docker-compose via pip
pip install --no-cache-dir docker-compose
docker-compose -v
